<?php
    class Melhoria extends CI_Controller{
        
        function index(){
            redirect(base_url("enquete/"));
        }
        
        function enquete($enquete){
            if (!$this->session->logged) {
                redirect(base_url("usuario/login/apenas_administrador"));
            }
            $this->load->view("base/header");
            
            $this->load->model("Melhoria_model", "melhoria");
            $data["melhorias"] = $this->melhoria->retrieve_from_db_enquete($enquete);
            
            $this->load->model("Enquete_model", "enquete");
            $this->enquete->retrieve_from_db_id($enquete);
            $data["enquete"] = $this->enquete;
            
            $this->load->view("melhoria/todas", $data);
            
            $this->load->view("base/footer");
        }
        
        function nova($enquete){
            if ($this->session->userdata("tipo") != "administrador") {
                redirect(base_url("usuario/login/apenas_administrador"));
            }
            
            $this->load->view("base/header");
            
            $data["id"] = $enquete; 
            
            if ($this->input->post() == null){
                $this->load->view("melhoria/nova", $data);
            } else {
                $this->load->model("Melhoria_model", "melhoria");
                
                $this->melhoria->titulo = ($this->input->post("titulo") != "") ? $this->input->post("titulo") : "Sem Nome";
                $this->melhoria->descricao = ($this->input->post("descricao") != "") ? $this->input->post("descricao") : "Melhoria Sem Nome nem Descrição";
                $this->melhoria->enquete = $enquete;
                
                $this->melhoria->id = $this->melhoria->create_on_db();
                
                $data["melhoria"] = $this->melhoria;
                
                $this->load->view("melhoria/sucesso", $data);
            }
            
            $this->load->view("base/footer");
        }
        
        function detalhes($melhoria){
            if (!$this->session->logged) {
                redirect(base_url("usuario/login/"));
            }
            
            $this->load->view("base/header");
            
            $this->load->model("Melhoria_model", "melhoria");
            $this->melhoria->retrieve_from_db_id($melhoria);
            
            $data["melhoria"] = $this->melhoria;
            
            $this->load->model("Imagem_model", "imagem");
            $data["imagens"] = $this->imagem->retrieve_from_db_melhoria($melhoria);
            
            $this->load->view("melhoria/detalhes", $data);
            
            $this->load->view("base/footer");
        }
        
        function remover($melhoria){
            if ($this->session->userdata("tipo") != "administrador") {
                redirect(base_url("usuario/login/apenas_administrador"));
            }
            
            $this->load->model("Melhoria_model", "melhoria");
            $this->melhoria->retrieve_from_db_id($melhoria);
            
            $enquete = $this->melhoria->enquete;
            
            $this->melhoria->delete_from_db();
            
            redirect(base_url("melhoria/enquete/" . $enquete));
        }
    }
?>