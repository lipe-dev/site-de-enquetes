<?php

class Enquete extends CI_Controller{
    function index(){
        if (!$this->session->userdata("logged")) {
            redirect(base_url("usuario/login/"));
        }
        
        $this->load->view("base/header");
        
        $this->load->model("Enquete_model", "enquete");
        $this->load->model("Resposta_model", "resposta");
        $this->load->model("Pergunta_model", "pergunta");
        $usuario = $this->session->id;
        
        $data["enquetes"] = $this->enquete->retrieve_from_db_all();
        $data["respondidas"] = array();
        $data["perguntas"] = array();
        $data["tipo"] = $this->session->tipo;
        
        if ($data["enquetes"]){
            foreach ($data["enquetes"] as $enquete){
                $perguntas = $this->pergunta->retrieve_from_db_enquete($enquete->id);
                if ($perguntas){
                    $respondida = 0;
                    foreach ($perguntas as $pergunta){
                        if ($this->resposta->retrieve_from_db_usuario_pergunta($usuario, $pergunta->id)){
                            $respondida++;
                        }
                    }
                    array_push($data["respondidas"], $respondida);
                } else {
                    array_push($data["respondidas"], 0);
                }
                array_push($data["perguntas"], $this->pergunta->retrieve_from_db_enquete($enquete->id));
            }
            $this->load->view("enquete/enquetes", $data);
        } else {
            $this->load->view("enquete/noenquetes");
        }
        
        $this->load->view("base/footer");
    }
    
    function nova(){
        if ($this->session->userdata("tipo") != "administrador") {
            redirect(base_url("usuario/login/apenas_administrador"));
        }
        
        $this->load->view("base/header");
        
        $this->load->model("Enquete_model", "enquete");
        
        if ($this->input->post() == null){
            $this->load->view("enquete/nova");
        } else {
            $this->enquete->titulo = ($this->input->post("titulo") != "") ? $this->input->post("titulo") : "Sem Título";
            $this->enquete->descricao = ($this->input->post("descricao") != "") ? $this->input->post("descricao") : "Enquete Sem Nome nem Descrição";
            $this->enquete->usuario = $this->session->id;
            
            $this->enquete->id = $this->enquete->create_on_db();
            
            $data["enquete"] = $this->enquete;
            
            $this->load->view("enquete/sucesso", $data);
        }
        
        $this->load->view("base/footer");
    }
    
    function editar($enquete, $message = ""){
        if ($this->session->userdata("tipo") != "administrador") {
            redirect(base_url("usuario/login/apenas_administrador"));
        }
        
        $this->load->view("base/header");
        
        $this->load->model("Enquete_model", "enquete");
        
        if ($this->enquete->retrieve_from_db_id($enquete)){
            if ($this->input->post() != null){
                $this->enquete->titulo = $this->input->post("titulo");
                $this->enquete->descricao = $this->input->post("descricao");
                
                $this->enquete->update_on_db();
                
                $this->load->view("enquete/atualizada");
            } 
        
            $data["enquete"] = $this->enquete;
            
            $this->load->model("Pergunta_model", "pergunta");
            $perguntas = $this->pergunta->retrieve_from_db_enquete($this->enquete->id);
            $data["perguntas"] = $perguntas;
            $this->load->view("enquete/editar", $data);
        } else {
            echo "<h1>Enquete invalida, tente novamente.</h1>";
        }
        
        if ($message != ""){
            $this->load->view("pergunta/excluida");
        }
        
        $this->load->view("base/footer");
    }
    
    function resultados($enquete){
        if ($this->session->userdata("tipo") != "administrador") {
            redirect(base_url("usuario/login/apenas_administrador"));
        }
        
        $this->load->view("base/header");
        
        $this->load->model("Enquete_model", "enquete");
        
        if ($this->enquete->retrieve_from_db_id($enquete)){
            $data["enquete"] = $this->enquete;
            
            $this->load->model("Pergunta_model", "pergunta");
            $perguntas = $this->pergunta->retrieve_from_db_enquete($this->enquete->id);
            $data["perguntas"] = $perguntas;
            
            $this->load->model("Resposta_model", "resposta");
            $this->load->model("Alternativa_model", "alternativa");
            
            $data["respostas"] = array();
            $data["quantidades"] = array();
            $data["alternativas"] = array();
            
            if ($perguntas){
                foreach ($perguntas as $pergunta){
                    $respostas = $this->resposta->retrieve_from_db_pergunta($pergunta->id);
                    
                    $data["respostas"][$pergunta->id] = $respostas;
                    
                    if ($respostas){
                        foreach ($respostas as $resposta){
                            if (isset($data["quantidades"][$pergunta->id][$resposta->valor])){
                                $data["quantidades"][$pergunta->id][$resposta->valor] ++;    
                            } else {
                                $data["quantidades"][$pergunta->id][$resposta->valor] = 1;
                            }
                        }
                    }
                    
                    $data["alternativas"][$pergunta->id] = $this->alternativa->retrieve_from_db_pergunta($pergunta->id);
                }
            }
            
            $this->load->view("enquete/resultados", $data);
        } else {
            echo "<h1>Enquete invalida, tente novamente.</h1>";
        }
        
        $this->load->view("base/footer");
    }
    
    public function responder($enquete){
        if (!$this->session->logged) {
            redirect(base_url("usuario/login/"));
        }
        
        $this->load->view("base/header");
        
        $this->load->model("Enquete_model", "enquete");
        $this->load->model("Pergunta_model", "pergunta");
        $this->load->model("Alternativa_model", "alternativa");
        
        if ($this->enquete->retrieve_from_db_id($enquete)){
            $perguntas = $this->pergunta->retrieve_from_db_enquete($enquete);
            if ($perguntas){
                $alternativas = array();
                foreach ($perguntas as $pergunta){
                    $alternativas[$pergunta->id] = $this->alternativa->retrieve_from_db_pergunta($pergunta->id); 
                }
            }
            
            $data["enquete"] = $this->enquete;
            $data["perguntas"]  = $perguntas;
            $data["alternativas"] = $alternativas;
            
            $this->load->view("enquete/responder", $data);
        }else {
            echo "<h1>Enquete invalida, tente novamente.</h1>";
        }
        
        $this->load->view("base/footer");
    }
    
    public function respondida($enquete){
        if (!$this->session->logged) {
            redirect(base_url("usuario/login/"));
        }
        
        if ($this->input->post() == null){
            redirect(base_url("enquete/responder/" . $enquete));
        }
        
        $this->load->view("base/header");
        
        $this->load->model("Enquete_model", "enquete");
        $this->load->model("Pergunta_model", "pergunta");
        $this->load->model("Resposta_model", "resposta");
        $this->load->model("Alternativa_model", "alternativa");
        
        if ($this->enquete->retrieve_from_db_id($enquete)){
            foreach ($this->input->post("pergunta") as $key => $val){
                 
                $this->pergunta->retrieve_from_db_id($key); 
                 
                $this->resposta->id = null;
                
                if ($this->pergunta->tipo == "fechada"){
                    $this->alternativa->retrieve_from_db_id($val);
                    $this->resposta->valor = $this->alternativa->resposta;
                    $this->resposta->alternativa = $val;
                } else {
                    $this->resposta->valor = $val;
                    $this->resposta->alternativa = null;
                }
                
                $this->resposta->pergunta = $key;
                $this->resposta->usuario = $this->session->id;
                
                $this->resposta->create_on_db();
            }  
            
            $this->load->view("enquete/obrigado");
        } else {
            echo "<h1>Enquete invalida, tente novamente.</h1>";
        }
        
        $this->load->view("base/footer");
    }
    
}

?>