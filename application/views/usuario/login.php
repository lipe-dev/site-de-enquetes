<div class="row clear pad-top-5 pad-bottom-5">
  
  <div class="col-4 push-4">
  
    <form method="post" action="<?= base_url("usuario/entrar"); ?>">
    
        <fieldset>
        
        <legend>Login</legend>
        
        <div class="col-12">
        
            <div class="field-group clear row pad-top-5 pad-bottom-5">
                <label for="username" class="col-4">Email</label>
                <input type="text" id="username" name="username" class="col-8"/>
            </div>
            <div class="field-group clear row pad-top-5 pad-bottom-5">
                <label for="password" class="col-4">Senha</label>
                <input type="password" name="password" class="col-8"/>
            </div>
            <div class="field-group clear row pad-top-5 pad-bottom-5">
                <input type="submit" value="Acessar o Sistema" class="col-12"/>
            </div>
        
        </div>
        
        </fieldset>
        
    
    </form>

  </div>

</div>

<?php 

    if ($erro == "credenciais_invalidas"){
        echo "Credenciais Invalidas, tente novamente";
    } else if ($erro == "apenas_administrador"){
        echo "Você não possui acesso à essa página, entre como um Administrador";
    }

?>