<div class="container">

    <div class="row clear pad-top-10">
        <h1>Melhoria Criada com Sucesso!</h1>
    </div>

    <div class="row clear pad-top-10">
        <h2><?= $melhoria->titulo; ?></h2>
        <p><?= $melhoria->descricao; ?></p>
    </div>

    <div class="row clear pad-top-10">
        <div class="col-4 menu-item">
            <a href="<?= base_url("imagem/adicionar/" . $melhoria->id); ?>">Adicionar Imagens</a>
        </div>
    </div>

</div>