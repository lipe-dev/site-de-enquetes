<div class="container">

    <div class="row clear pad-top-10">
        <h1>Nova Melhoria</h1>    
        
        <p>Preencha os dados abaixo para criar uma nova melhoria.</p>
    </div>

    <form action="<?= base_url("melhoria/nova/" . $id); ?>" method="post">
            
        <div class="field-group row clear pad-top-5">
            <label for="titulo" class="col-3 text-right">Titulo</label>
            <input type="text" id="nome" name="titulo" class="col-9"/>
        </div>
        
        <div class="field-group row clear pad-top-5">
            <label for="descricao" class="col-3 text-right">Descrição</label>
            <input type="text" id="descricao" name="descricao" class="col-9"/>
        </div>
            
        <div class="row clear pad-top-10">
            <input type="submit" value="Criar Melhoria" class="col-4 push-8"/>
        </div>
            
    </form>

</div>