<div class="container">

    <div class="row clear pad-top-10">
        <h1><?= $enquete->titulo; ?></h1>
        <p><?= $enquete->descricao; ?></p>
    </div>

    <div class="row clear pad-top-10">
        <div class="col-4 menu-item">
            <a href="<?= base_url("melhoria/nova/" . $enquete->id); ?>">Nova Melhoria</a>
        </div>
    </div>

    <div class="row clear pad-top-10">
        <h2>Melhorias Geradas: </h2>
    </div>
    
    <div class="row clear pad-top-10">
        <table>
            <tr>
                <th>Melhoria</th>
                <th>Descrição</th>
                <th>Ações</th>
            </tr>
        <?php 
            if ($melhorias) { 
                foreach($melhorias as $melhoria){
        ?>
                    <tr>
                        <td><?= $melhoria->titulo; ?></td>
                        <td><?= $melhoria->descricao; ?></td>
                        <td>
                            <a href="<?= base_url("melhoria/detalhes/" . $melhoria->id); ?>">Detalhes</a>
                            <a href="<?= base_url("melhoria/remover/" . $melhoria->id); ?>">Remover</a>
                        </td>
                    </tr>
        <?php 
                }
            } else { ?>
                <tr>
                    <td>Não há melhorias registradas</td>
                    <td>Clique no botão para adicionar melhorias</td>
                </tr>
            <?php }
        ?>
        </table>
    </div>
    
    </form>

</div>