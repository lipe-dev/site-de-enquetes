<script type="text/javascript" src="<?= base_url() . "assets/getbase/dist/js/vendor/jquery.min.js"; ?>"></script>
<script type="text/javascript" src="<?= base_url() . "assets/getbase/dist/js/default.js"; ?>"></script>

</body>
</html>


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->

<script>
    $("#select").change(function(){
       if ($(this).val() === "fechada"){
           adicionarAlternativas();
       } else {
           removerAlternativas();
       }
    });
    
    function adicionarAlternativas(){
        $("#form").fadeIn(300);
        $("#form").append(
            "<div class='alternativa row clear pad-top-5 field-group'>" +
            "<button id='addmore' type='button' class='col-10 push-1'>Adicionar Mais</button>" +
            "<label for='alternativa' class='col-3 text-right margin-top'>Alternativa</label>" +
            "<input type='text' name='alternativas[]' id='alternativa' class='col-8 margin-top'/></div>"
        );
        
        $("#addmore").click(function(){
            adicionarAlternativa();
        });
    }
    
    function adicionarAlternativa(){
        $("#form").append("<div class='alternativa row clear pad-top-5'>" +
            "<label for='alternativa' class='col-3 text-right'>Alternativa</label>" +
            "<input type='text' name='alternativas[]' id='alternativa' class='col-8' />"
        )
    }
    
    function removerAlternativas(){
        $("#form").fadeOut(300);
        $(".alternativa").remove()
    }
    
    $("#moreimages").click(function(){
        $("#images").clone().appendTo("form");
    });
</script>