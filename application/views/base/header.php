<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Base</title>

  <meta name="description" content="">
  <meta name="theme-color" content="#0D47A1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() . "assets/getbase/dist/img/apple-touch-icon.png"; ?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() . "assets/getbase/dist/img/apple-touch-icon-72x72.png"; ?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() . "assets/getbase/dist/img/apple-touch-icon-114x114.png"; ?>">

  <link rel="stylesheet" href="<?= base_url() . "assets/getbase/dist/css/styles.css"; ?>">
  <link rel="stylesheet" href="<?= base_url() . "assets/getbase/dist/css/custom.css"; ?>">

</head>
<body>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.bundle.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/randomcolor/0.4.4/randomColor.min.js"></script>
  
  <div class="navbar">
    <div class="container">
      <div class="clear row">
        <div class="col-4 logo">Sistema de Enquetes</div>
        
        <div class="col-8">
          <div class="text-center tab">
            <a href="<?= base_url("usuario"); ?>">Home</a>
          </div>
          <div class="text-center tab">
            <a href="<?= base_url("enquete"); ?>">Enquetes</a>
          </div>
          <div class="text-center tab">
            <a href="<?= base_url("usuario/todos"); ?>">Usuários</a>
          </div>
        </div>
      </div>
    </div>
  </div>