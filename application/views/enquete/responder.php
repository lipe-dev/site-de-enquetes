<div class="container">

    <div class="clear row pad-top-10">
        <h1><?= $enquete->titulo; ?></h1>
    </div>
    
    <div class="clear row pad-top-10">
        <p><?= $enquete->descricao; ?></p>
    </div>
    
    <form action="<?= base_url("enquete/respondida/" . $enquete->id); ?>" method="post">
    
    <?php 
        foreach ($perguntas as $pergunta) { 
    ?>
    
        <div class="row clear pad-top-10">
            <p><b>Pergunta: </b><?= $pergunta->pergunta; ?></p>
        </div>
        
        <?php
            if ($pergunta->tipo == "fechada") { ?>
                
                <div class="clear row field-group">
                    <label for="selec" class="col-2">Resposta: </label>
                    <select id="selec" name="pergunta[<?= $pergunta->id; ?>]" class="col-10">
                 
                        <?php
                        foreach ($alternativas[$pergunta->id] as $alternativa){ ?>
                            
                            <option value="<?= $alternativa->id; ?>"><?= $alternativa->resposta?></option>
                                                  
                        <?php } ?>
                 
                    </select>
                </div>
            <?php
            } else { ?>
                
                <div class="clear row pad-top-10 field-group">
                    <label for="selec" class="col-2">Resposta: </label>
                    <input id="selec" type="text" name="pergunta[<?= $pergunta->id; ?>]" class="col-10"/>
                </div>
                    
            <?php
            }
        }
    ?>
    
    <div class="row clear pad-top-10">
        <input type="submit" value="Enviar Respostas" class="col-4"/>
    </div>
    
    </form>

</div>