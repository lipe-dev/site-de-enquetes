<div class="container">

    <div class="row clear pad-top-10">
        <h1>Editar Enquete</h1>    
        
        <p>Preencha os dados abaixo para editar a enquete.</p>
    </div>

    <form action="<?= base_url("enquete/editar/" . $enquete->id); ?>" method="post">
            
        <div class="field-group row clear pad-top-5">
            <label for="titulo" class="col-3 text-right">Titulo</label>
            <input type="text" id="nome" name="titulo" class="col-9" value="<?= $enquete->titulo; ?>"/>
        </div>
        
        <div class="field-group row clear pad-top-5">
            <label for="descricao" class="col-3 text-right">Descrição</label>
            <input type="text" id="descricao" name="descricao" class="col-9"  value="<?= $enquete->descricao; ?>"/>
        </div>
            
        <div class="row clear pad-top-10">
            <input type="submit" value="Salvar Enquete" class="col-4 push-8"/>
        </div>
            
    </form>
    
    <div class="row clear pad-top-10">
        <h2>Perguntas: </h2>
    </div>

    <div class="row clear pad-top-10">
        <div class="col-4 menu-item">
            <a href="<?= base_url("pergunta/adicionar/" . $enquete->id); ?>">Adicionar Perguntas</a>
        </div>
    </div>

    <div class="row clear pad-top-10">
        <table>
            <tr>
                <th>Pergunta</th>
                <th>Tipo</th>
                <th>Ações</th>
            </tr>
        <?php 
            if ($perguntas) { 
                foreach($perguntas as $pergunta){
        ?>
                    <tr>
                        <td><?= $pergunta->pergunta ?></td>
                        <td><?= $pergunta->tipo ?></td>
                        <td><a href="<?= base_url("pergunta/remover/" . $pergunta->id); ?>">Remover</a></td>
                    </tr>
        <?php 
                }
            } 
        ?>
        </table>
    </div>

</div>