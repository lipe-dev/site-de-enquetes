<?php

class Usuario_model extends CI_Model{
    public function __construct()
        {
                parent::__construct();
        }
        
    public $id;
    public $nome;
    public $email;
    public $senha;
    public $registro;
    public $tipo;
    
    public function create_on_db(){
        $this->db->insert("usuarios", $this);
    }
    
    public function delete_from_db(){
        $this->db->where("id", $this->id);
        $this->db->delete("usuarios");
    }
    
    public function check_all_filled(){
        return $this->nome != "" 
        && $this->email != ""
        && $this->senha != ""
        && $this->registro != ""
        && $this->tipo != "";
        
    }
    
    public function retrieve_from_db_all(){
        $query = $this->db->get("usuarios");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $usuarios = array();

        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->nome = $row->nome;
            $this->email = $row->email;
            $this->senha = $row->senha;
            $this->registro = $row->registro;
            $this->tipo = $row->tipo;
            
            array_push($usuarios, clone $this);
        }
        
        return $usuarios;
    }
    
    public function retrieve_from_db_id($id){
        $this->db->where("id", $id);
        $query = $this->db->get("usuarios");
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->nome = $row->nome;
            $this->email = $row->email;
            $this->senha = $row->senha;
            $this->registro = $row->registro;
            $this->tipo = $row->tipo;
            
            return true;
        }
        
        return false;
    }
    
    public function retrieve_from_db_credentials($mEmail, $mPassword){
        $this->db->where("email", $mEmail);
        $query = $this->db->get("usuarios");
        
        foreach ($query->result() as $row){
            if (password_verify($mPassword, $row->senha)){

                $this->id = $row->id;
                $this->nome = $row->nome;
                $this->email = $row->email;
                $this->senha = $row->senha;
                $this->registro = $row->registro;
                $this->tipo = $row->tipo;
                
                return true;
            }
        }
        
        return false;
    }
}

?>