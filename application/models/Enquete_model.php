<?php

class Enquete_model extends CI_Model{
    public function __construct()
        {
                parent::__construct();
        }
        
    public $id;
    public $titulo;
    public $descricao;
    
    public $usuario;
    
    public function create_on_db(){
        $this->db->insert("enquetes", $this);
        return $this->db->insert_id();
    }
    
    public function update_on_db(){
        $this->db->where("id", $this->id);
        $this->db->update("enquetes", $this);
    }
    
    public function retrieve_from_db_all(){
        $query = $this->db->get("enquetes");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $enquetes = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->titulo = $row->titulo;
            $this->descricao = $row->descricao;
            $this->usuario = $row->usuario;
            
            array_push($enquetes, clone $this);
        }
        
        return $enquetes;
    }
    
    public function retrieve_from_db_usuario($usuario){
        $this->db->where("usuario", $usuario);
        $query = $this->db->get("enquetes");
        
        if ($query->num_rows() < 1){
            return false;
        }
        
        $enquetes = array();
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->titulo = $row->titulo;
            $this->descricao = $row->descricao;
            $this->usuario = $row->usuario;
            
            array_push($enquetes, clone $this);
        }
        
        return $enquetes;
    }
    
    public function retrieve_from_db_id($id){
        $this->db->where("id", $id);
        $query = $this->db->get("enquetes");
        
        
        foreach ($query->result() as $row){
            $this->id = $row->id;
            $this->titulo = $row->titulo;
            $this->descricao = $row->descricao;
            $this->usuario = $row->usuario;
            
            return true;
        }
        
        return false;
    }
}

?>